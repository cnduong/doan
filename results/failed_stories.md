## happy path 1 (/var/folders/mh/hsq7v_sd1lv7xvxct81g2ykw0000gn/T/tmpvfatikgl/f0af933c18364090b6224c434fd4198d_conversation_tests.md)
* greet: hello there!   <!-- predicted: bait_question: hello there! -->
    - utter_greet   <!-- predicted: action_baitingboy -->
    - action_listen   <!-- predicted: action_baitingboy -->
* mood_great: amazing   <!-- predicted: affim: amazing -->
    - utter_happy   <!-- predicted: action_iamboy -->
    - action_listen   <!-- predicted: action_iamboy -->


## happy path 2 (/var/folders/mh/hsq7v_sd1lv7xvxct81g2ykw0000gn/T/tmpvfatikgl/f0af933c18364090b6224c434fd4198d_conversation_tests.md)
* greet: hello there!   <!-- predicted: bait_question: hello there! -->
    - utter_greet   <!-- predicted: action_baitingboy -->
    - action_listen   <!-- predicted: action_baitingboy -->
* mood_great: amazing   <!-- predicted: affim: amazing -->
    - utter_happy   <!-- predicted: action_iamboy -->
    - action_listen   <!-- predicted: action_iamboy -->
* goodbye: bye-bye!   <!-- predicted: intent_bye: bye-bye! -->
    - utter_goodbye   <!-- predicted: action_bye -->
    - action_listen   <!-- predicted: action_bye -->


## sad path 1 (/var/folders/mh/hsq7v_sd1lv7xvxct81g2ykw0000gn/T/tmpvfatikgl/f0af933c18364090b6224c434fd4198d_conversation_tests.md)
* greet: hello   <!-- predicted: intent_hello: hello -->
    - utter_greet   <!-- predicted: action_hello -->
    - action_listen   <!-- predicted: action_hello -->
* mood_unhappy: not good   <!-- predicted: chitchat20: not good -->
    - utter_cheer_up   <!-- predicted: action_chitchat20 -->
    - utter_did_that_help   <!-- predicted: action_chitchat20 -->
    - action_listen   <!-- predicted: action_chitchat20 -->
* affirm: yes   <!-- predicted: affim: yes -->
    - utter_happy   <!-- predicted: action_iamboy -->
    - action_listen   <!-- predicted: action_iamboy -->


## sad path 2 (/var/folders/mh/hsq7v_sd1lv7xvxct81g2ykw0000gn/T/tmpvfatikgl/f0af933c18364090b6224c434fd4198d_conversation_tests.md)
* greet: hello   <!-- predicted: intent_hello: hello -->
    - utter_greet   <!-- predicted: action_hello -->
    - action_listen   <!-- predicted: action_hello -->
* mood_unhappy: not good   <!-- predicted: chitchat20: not good -->
    - utter_cheer_up   <!-- predicted: action_chitchat20 -->
    - utter_did_that_help   <!-- predicted: action_chitchat20 -->
    - action_listen   <!-- predicted: action_chitchat20 -->
* deny: not really   <!-- predicted: iamfine: not really -->
    - utter_goodbye   <!-- predicted: action_response_iamfine -->
    - action_listen   <!-- predicted: action_response_iamfine -->


## sad path 3 (/var/folders/mh/hsq7v_sd1lv7xvxct81g2ykw0000gn/T/tmpvfatikgl/f0af933c18364090b6224c434fd4198d_conversation_tests.md)
* greet: hi   <!-- predicted: about_me: hi -->
    - utter_greet   <!-- predicted: action_aboutme -->
    - action_listen   <!-- predicted: action_aboutme -->
* mood_unhappy: very terrible   <!-- predicted: iamboy: very terrible -->
    - utter_cheer_up   <!-- predicted: action_iamboy -->
    - utter_did_that_help   <!-- predicted: action_iamboy -->
    - action_listen   <!-- predicted: action_iamboy -->
* deny: no   <!-- predicted: chitchat8: no -->
    - utter_goodbye   <!-- predicted: action_chitchat8 -->
    - action_listen   <!-- predicted: action_chitchat8 -->


## say goodbye (/var/folders/mh/hsq7v_sd1lv7xvxct81g2ykw0000gn/T/tmpvfatikgl/f0af933c18364090b6224c434fd4198d_conversation_tests.md)
* goodbye: bye-bye!   <!-- predicted: intent_bye: bye-bye! -->
    - utter_goodbye   <!-- predicted: action_bye -->
    - action_listen   <!-- predicted: action_bye -->


## bot challenge (/var/folders/mh/hsq7v_sd1lv7xvxct81g2ykw0000gn/T/tmpvfatikgl/f0af933c18364090b6224c434fd4198d_conversation_tests.md)
* bot_challenge: are you a bot?   <!-- predicted: about_me: are you a bot? -->
    - utter_iamabot   <!-- predicted: action_aboutme -->
    - action_listen   <!-- predicted: action_aboutme -->


