import datetime
import numpy as np

class DataInformation(object):
    """docstring for DataInformation"""
    def __init__(self):
        super(DataInformation, self).__init__()

    def normalize(self, query_json):
        result = {}
        birthday = query_json.get('birthday')
        age = datetime.datetime.now().year - int(birthday.split("/")[-1])
        print("tuoi", age)
        result['age'] = self.get_age(age)
        result['bmi'] = self.get_bmi(query_json.get('weight'), query_json.get('height'))
        print("goi tinh va chieu cao",query_json.get('gender'), query_json.get('height'))
        result['height'] = self.get_height(query_json.get('height'), query_json.get('gender'))
        result['job'] = self.get_job(query_json.get('job'))
        list_key = ['movie', 'travel', 'shopping', 'swimming', 'camping', 'pet']
        for key in list_key:
            result[key] = query_json.get(key)
        return result

    def get_bmi(self, weight, height):
        weight = float(weight)
        height = float(height)
        bmi = weight*1.0 / height / height * 100 * 100
        if bmi < 18.5:
            return -1
        if bmi >= 25:
            return 1
        return 0

    def get_height(self, height, gender):
        height = float(height)
        if gender == 'nam':
            if height >= 170:
                return 1
            if height >= 165 and height < 170:
                return 0
            return -1
        else :
            if height >= 160:
                return 1
            if height < 155:
                return -1
            return 0

    def get_job(self, job):
        job = job.lower()
        list_job = ['kỹ thuật', 'y tế', 'kinh tế', 'giáo dục', 'nhà nước', 'quân đội', 'khác']
        if job not in list_job:
            return -1
        list_job = np.array(list_job)
        return np.where(list_job == job)[0][0]
    
    def get_age(self, age):
        if age < 20:
            return 0
        if age >= 20 and age <= 30:
            return 1
        if age >= 31 and age <= 40:
            return 2
        return 3