import logging
import uuid
import inspect
import rasa

import io
import urllib.request
from PIL import Image

from sanic import Blueprint, response
from sanic.request import Request
from socketio import AsyncServer
from typing import Text, List, Dict, Any, Optional, Callable, Iterable, Awaitable
from asyncio import Queue, CancelledError
from rasa.core.channels.channel import UserMessage, OutputChannel, CollectingOutputChannel, InputChannel, \
    QueueOutputChannel

import asyncio
import inspect
import json
import logging
import uuid
from asyncio import Queue, CancelledError
from sanic import Sanic, Blueprint, response
from sanic.request import Request
from typing import Text, List, Dict, Any, Optional, Callable, Iterable, Awaitable

import rasa.utils.endpoints
from rasa.cli import utils as cli_utils
from rasa.constants import DOCS_BASE_URL
from rasa.core import utils
from sanic.response import HTTPResponse
from rasa.core.domain import Domain
import mysql.connector as mysql_db
import csv

from rasa.core.domain import TemplateDomain
from rasa.core.training.dsl import StoryFileReader
from rasa.core.training.visualization import visualize_stories
from rasa.core.training import visualization
import asyncio
import random
import time
from rasa.core.training.structures import StoryGraph
import networkx as nx
import os
import pickle
import json
from src.api.format_information import DataInformation

from config import *
from sklearn.metrics.pairwise import euclidean_distances

logger = logging.getLogger(__name__)


class RestInputCustom(InputChannel):
    """A custom http input channel.

    This implementation is the basis for a custom implementation of a chat
    frontend. You can customize this to send messages to Rasa Core and
    retrieve responses from the agent."""

    def __init__(self):
        with open(PATH_CONFIG, "r") as file_config:
            self.config = json.load(file_config)
        self.data_format_information = DataInformation()
        self.name_table_account = self.config.get("database").get("name_table_account")
        self.name_table_user = self.config.get("database").get("name_table_user")
        self.path_domain = self.config.get("path_domain")
        self.path_domain = PATH_DOMAIN
        self.domain = Domain.load(self.path_domain)
        self.connect = mysql_db.connect(host=self.config.get("database").get("host"),
                                        username=self.config.get("database").get("username"),
                                        password=self.config.get("database").get("password"),
                                        db=self.config.get("database").get("db"),
                                        port=self.config.get("database").get("port"))

        # self.connect.autocommit = True
        with open(PATH_CONFIG_USER_PARAMS, "r") as file_slot:
            self.reader_config_slots = json.load(file_slot)
        with open(PATH_CONFIG_GRAPH, "rb") as file_config:
            self.result_stories = pickle.load(file_config)
        with open(PATH_CONFIG_TEMPLATE_ACTION, "r") as file:
            self.data_template = json.load(file)

        # cursor = self.connect.cursor()

        # insert1 = "INSERT INTO Artists(id, ten, dia chi, sdt) VALUES('1', 'JazDuongz', 'Ha noi', '111' );"
        # cursor.execute(insert1)
        # self.connect.commit()
        # self.connect.close()
        # x_train, y_train, g_train = self.train_knn()
        # peple_match, value_min = self.compare_similar(y_train[0], x_train[0],g_train[0] , x_train, y_train, g_train)
        # print ("people match: ", peple_match)
        # print("value: ",value_min)
        # exit()
        # x)train la du lieu sau khi bien do
        # y train la id_user
        # g_train la danh sach gioi tinh


    def train_knn(self, age_condition, height_condition, weight_condition, job_condition):
        self.connect.ping(reconnect=True)
        mycursor = self.connect.cursor()
        sql =  "SELECT * FROM "
        sql += self.name_table_user; 
        
        mycursor.execute(sql)
        myresult = mycursor.fetchall()
        x_train = []
        y_train = []
        g_train = [] 

    
        x_data_list = []
        for x in myresult:
            query_json = {'gender': x[4], 'birthday' : x[5],'height': x[6], 'weight': x[7], 'job': x[8], 'movie': x[9],'travel' : x[10], 'shopping': x[11], 'swimming': x[12],'camping': x[13], 'pet': x[14]}
            data_json = self.data_format_information.normalize(query_json)
            x_data = data_json.values()
            x_data_list = list(x_data)
            
            
            if x_data_list[0] == age_condition and x_data_list[1] == weight_condition and x_data_list[2] == height_condition and x_data_list[3] == job_condition:
                x_train.append(list(x_data))
                y_train.append(x[0])
                g_train.append(x[4])
        return x_train, y_train, g_train

    def compare_similar(self, id_query, x_query, gender_query, x_train, y_train, g_train):
        distance = euclidean_distances([x_query], x_train)[0]
        index_min = -1
        value_min = 10000
        for i, value in enumerate(distance):
            print("khoảng cách",value)
            if y_train[i] != id_query and value_min > value and gender_query != g_train[i]:
                value_min = value
                index_min = y_train[i]

        print("khoảng cách nhỏ nhất", value_min)
        print("user_id", index_min)
        return index_min, value_min

    def get_height_value(self, height_condition):
        if height_condition == "Cao":
            return 1
        if height_condition == "Thấp":
            return -1
        return 0
    
    def get_age_value(self, age_condition):
        if age_condition == "Dưới 20 tuổi":
            return 0
        if age_condition == "Từ 20 - 30 tuổi":
            return 1
        if age_condition == "Từ 31 - 40 tuổi":
            return 2
        return 3

    def get_weight_value(self, weight_condition):
        if weight_condition == "Béo":
            return 1
        if weight_condition == "Gầy":
            return -1
        return 0
    
    def get_job_value(self, job_condition):
        if job_condition == "Kỹ thuật":
            return 0
        if job_condition == "Y tế":
            return 1
        if job_condition == "Kinh tế":
            return 2
        if job_condition == "Nhà nước":
            return 3
        if job_condition == "Quân đội":
            return 4
        return 5

    @classmethod
    def name(cls) -> Text:
        return "rest_custom"

    @staticmethod
    async def on_message_wrapper(
            on_new_message: Callable[[UserMessage], Awaitable[Any]],
            text: Text,
            queue: Queue,
            sender_id: Text,
            input_channel: Text,
            metadata: Optional[Dict[Text, Any]],
    ) -> None:
        collector = QueueOutputChannel(queue)

        message = UserMessage(
            text, collector, sender_id, input_channel=input_channel, metadata=metadata
        )
        await on_new_message(message)

        await queue.put("DONE")  # pytype: disable=bad-return-type

    async def _extract_sender(self, req: Request) -> Optional[Text]:
        return req.json.get("sender", None)

    # noinspection PyMethodMayBeStatic
    def _extract_message(self, req: Request) -> Optional[Text]:
        return req.json.get("message", None)

    def _extract_input_channel(self, req: Request) -> Text:
        return req.json.get("input_channel") or self.name()

    def stream_response(
            self,
            on_new_message: Callable[[UserMessage], Awaitable[None]],
            text: Text,
            sender_id: Text,
            input_channel: Text,
            metadata: Optional[Dict[Text, Any]],
    ) -> Callable[[Any], Awaitable[None]]:
        async def stream(resp: Any) -> None:
            q = Queue()
            task = asyncio.ensure_future(
                self.on_message_wrapper(
                    on_new_message, text, q, sender_id, input_channel, metadata
                )
            )
            result = None  # declare variable up front to avoid pytype error
            while True:
                result = await q.get()
                if result == "DONE":
                    break
                else:
                    await resp.write(json.dumps(result) + "\n")
            await task

        return stream  # pytype: disable=bad-return-type

    def blueprint(
            self, on_new_message: Callable[[UserMessage], Awaitable[None]]
    ) -> Blueprint:
        custom_webhook = Blueprint(
            "custom_webhook_{}".format(type(self).__name__),
            inspect.getmodule(self).__name__,
        )

        # noinspection PyUnusedLocal
        @custom_webhook.route("/", methods=["GET"])
        async def health(request: Request) -> HTTPResponse:
            return response.json({"status": "ok"})

        # getIntents
        @custom_webhook.route("/getIntents", methods=["GET"])
        async def get_intents(request: Request) -> HTTPResponse:
            list_intents = self.domain.intents
            return response.json({
                "status": 0,
                "msg": "Success",
                "intents": list_intents,
            })

        # getEntitites
        @custom_webhook.route("/getEntities", methods=["GET"])
        async def get_entities(request: Request) -> HTTPResponse:
            list_entities = self.domain.entities
            print(self.domain)
            return response.json({
                "status": 0,
                "msg": "Success",
                "entities": list_entities,
            })

        # getActions
        @custom_webhook.route("/getActions", methods=["GET"])
        async def get_actions(request: Request) -> HTTPResponse:
            list_actions = self.domain.user_actions
            return response.json({
                "status": 0,
                "msg": "Success",
                "actions": list_actions,
            })

        # login
        @custom_webhook.route("/login", methods=["POST"])
        async def login (request: Request) -> HTTPResponse:
            self.connect.ping(reconnect=True)
            username = request.json.get("username")
            password = request.json.get("password")

            mycursor = self.connect.cursor()

            sql =  "SELECT * FROM "
            sql += self.name_table_account; 
            sql += " WHERE username like %s AND password like %s"
            mycursor.execute(sql, (username, password))
            myresult = mycursor.fetchall()
            count = 0
            result = []
            for x in myresult:
                count += 1
                break
           
            if count != 0:
                sqluser = "SELECT * FROM "
                sqluser += self.name_table_user
                sqluser += " WHERE ID = {}".format(int (myresult[0][3]))
                print ("sql user: " , sqluser)
                #mycursor.execute(sqluser, int (myresult[0][3]))
                mycursor.execute(sqluser)

                user_result = mycursor.fetchall()

                return response.json({
                "status": 0,
                "msg": "Success",
                "name": user_result[0][1],
                "address": user_result[0][2],
                "phone": user_result[0][3],
                "gender": user_result[0][4],
                "birthday": user_result[0][5],
                "height": user_result[0][6],
                "weight": user_result[0][7],
                "job": user_result[0][8],
                "movie": user_result[0][9],
                "travel": user_result[0][10],
                "shopping": user_result[0][11],
                "swimming": user_result[0][12],
                "camping": user_result[0][13],
                "pet": user_result[0][14],
                "other_hobby": user_result[0][15]
            })
            else:
                 return response.json({
                "status": 0,
                "msg": "Failure",
            })

        # register
        @custom_webhook.route("/register", methods = ["POST"])
        async def register (request: Request) -> HTTPResponse:
            self.connect.ping(reconnect=True)
            
            username = request.json.get("username")
            password = request.json.get("password")

            name = request.json.get("name")
            address = request.json.get("address")
            phone = request.json.get("phone")
            gender = request.json.get("gender")
            birthday = request.json.get("birthday")
            height = request.json.get("height")
            weight = request.json.get("weight")
            job = request.json.get("job")
            movie = request.json.get("movie")
            travel = request.json.get("travel")
            shopping = request.json.get("shopping")
            swimming = request.json.get("swimming")
            camping = request.json.get("camping")
            pet = request.json.get("pet")
            other_hobby = request.json.get("other_hobby")

            print("username", username)
            print("password", password)
            print("pet", pet)

            
            mycursor = self.connect.cursor()

            sqlCheckExists =  "SELECT * FROM "
            sqlCheckExists += self.name_table_account; 
            sqlCheckExists += " WHERE username like %s"
            mycursor.execute(sqlCheckExists, (str(username),))
            myresult = mycursor.fetchall()
            if len(myresult) != 0:
                return response.json({
                "status": 0,
                "msg": "Failure",
            })
            else :

                sql = "INSERT INTO "
                sql += self.name_table_user
                sql += " (name, address, phone, gender, birth_day, height, weight, job, movie, travel, shopping, swimming, camping, pet, other_hobby) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                
                mycursor.execute(sql, (name, address, phone, gender, birthday, height, weight, job, movie, travel, shopping, swimming, camping, pet, other_hobby))
                print(sql)
                self.connect.commit()
                sqlMaxID =  "SELECT MAX(ID) FROM "
                sqlMaxID += self.name_table_user

                mycursor.execute(sqlMaxID)

                myresultMaxID = mycursor.fetchall()

                maxID = myresultMaxID[0][0]

                print("maxID",maxID)
                sqlAccount = "INSERT INTO "
                sqlAccount += self.name_table_account
                sqlAccount += " (username, password, user_id) VALUES (%s, %s, %s)"

                mycursor.execute(sqlAccount, (username, password, maxID))
                self.connect.commit()
                print("sqlAccount",sqlAccount)
                return response.json({
                "status": 0,
                "msg": "Success",    
            })
                # else:
                #      return response.json({
                #     "status": 0,
                #     "msg": "Failure",
                # })
        


        #find friend
        @custom_webhook.route("/find_friend", methods = ["POST"])
        async def find_friend (request: Request) -> HTTPResponse:
            self.connect.ping(reconnect=True)
            job_condition = request.json.get("job")
            age_condition = request.json.get("age")
            height_condition = request.json.get("height")
            weight_condition = request.json.get("weight")
            username = request.json.get("username")

            job_condition_value = self.get_job_value(job_condition)
            age_condition_value = self.get_age_value(age_condition)
            height_condition_value = self.get_height_value(height_condition)
            weight_condition_value = self.get_weight_value(weight_condition)

            print(job_condition_value, height_condition_value, weight_condition_value,age_condition_value, username)
            mycursor = self.connect.cursor()

            sql =  "SELECT * FROM "
            sql += self.name_table_account; 
            sql += " WHERE username like %s "
            print("sql user find match ",sql)
            mycursor.execute(sql, (username,))

            myresult = mycursor.fetchall()

            sqluser = "SELECT * FROM "
            sqluser += self.name_table_user
            sqluser += " WHERE ID = {}".format(int (myresult[0][3]))
            print ("sql user: " , sqluser)
                #mycursor.execute(sqluser, int (myresult[0][3]))
            mycursor.execute(sqluser)

            user_result = mycursor.fetchall()
            # query_json_need = {'birthday' : user_result[0][5],'height': user_result[0][6], 'weight': user_result[0][7], 'job': user_result[0][8], 'movie': user_result[0][9],'travel' : user_result[0][10], 'shopping': user_result[0][11], 'swimming': user_result[0][12],'camping': user_result[0][13], 'pet': user_result[0][14]}
            # data_json_response = self.data_format_information.normalize(query_json_need)
            # x_data_need = data_json_response.values()
            # x_train_need = []
            # x_train_need.append(list(x_data_need))
            # x la du lieu bien doi, y la user_id, g la gioitinh
            x_train, y_train, g_train = self.train_knn(age_condition_value,height_condition_value,weight_condition_value,job_condition_value)
            print("user_result ", user_result[0][0],user_result[0][4], int (myresult[0][3]))
            print("so luong",len(x_train))
            if len(x_train) != 0:
                # lay du lieu thuoc tinh user
                query_json = {'gender': user_result[0][4], 'birthday' : user_result[0][5],'height': user_result[0][6], 'weight': user_result[0][7], 'job': user_result[0][8], 'movie': user_result[0][9],'travel' : user_result[0][10], 'shopping': user_result[0][11], 'swimming': user_result[0][12],'camping': user_result[0][13], 'pet': user_result[0][14]}
                data_json = self.data_format_information.normalize(query_json)
                x_data = data_json.values()
                x_data_list = list(x_data)
                peple_match, value_min = self.compare_similar(user_result[0][0], x_data_list, user_result[0][4], x_train, y_train, g_train)
                print ("people match: ", peple_match)


                sqluser_match = "SELECT * FROM "
                sqluser_match += self.name_table_user
                sqluser_match += " WHERE ID = {}".format(int (peple_match))
                print ("sql user: " , sqluser)
                    #mycursor.execute(sqluser, int (myresult[0][3]))
                mycursor.execute(sqluser_match)

                user_result_match = mycursor.fetchall()

                sqlaccount_match = "SELECT * FROM "
                sqlaccount_match += self.name_table_account
                sqlaccount_match += " WHERE user_id = {}".format(int (peple_match))
                print ("sqlaccount_match: " , sqlaccount_match)
                    #mycursor.execute(sqluser, int (myresult[0][3]))
                mycursor.execute(sqlaccount_match)

                account_result_match = mycursor.fetchall()

                # list_key = ['age','birthday','weight','height','bmi','job','movie','travel','shopping','swimming','camping','pet']
                # query_json = {}
                # for key in list_key:
                #     query_json[key] = request.json.get(key)
                # out = self.data_format_information.normalize(query_json)
                # print (out)
                return response.json({
                    "status": 0,
                    "msg": "Success",
                    "name": user_result_match[0][1],
                    "address": user_result_match[0][2],
                    "phone": user_result_match[0][3],
                    "gender": user_result_match[0][4],
                    "birthday": user_result_match[0][5],
                    "height": user_result_match[0][6],
                    "weight": user_result_match[0][7],
                    "job": user_result_match[0][8],
                    "movie": user_result_match[0][9],
                    "travel": user_result_match[0][10],
                    "shopping": user_result_match[0][11],
                    "swimming": user_result_match[0][12],
                    "camping": user_result_match[0][13],
                    "pet": user_result_match[0][14],
                    "other_hobby": user_result_match[0][15],
                    "username": account_result_match[0][1]
            })
            else:
                 return response.json({
                "status": 0,
                "msg": "Failure",
            })
           
            # # lay du lieu thuoc tinh user
            # query_json = {'gender': user_result[0][4], 'birthday' : user_result[0][5],'height': user_result[0][6], 'weight': user_result[0][7], 'job': user_result[0][8], 'movie': user_result[0][9],'travel' : user_result[0][10], 'shopping': user_result[0][11], 'swimming': user_result[0][12],'camping': user_result[0][13], 'pet': user_result[0][14]}
            # data_json = self.data_format_information.normalize(query_json)
            # x_data = data_json.values()
            # x_data_list = list(x_data)
            # peple_match, value_min = self.compare_similar(user_result[0][0], x_data_list, user_result[0][4], x_train, y_train, g_train)
            # print ("people match: ", peple_match)


            # sqluser_match = "SELECT * FROM "
            # sqluser_match += self.name_table_user
            # sqluser_match += " WHERE ID = {}".format(int (peple_match))
            # print ("sql user: " , sqluser)
            #     #mycursor.execute(sqluser, int (myresult[0][3]))
            # mycursor.execute(sqluser_match)

            # user_result_match = mycursor.fetchall()

            # sqlaccount_match = "SELECT * FROM "
            # sqlaccount_match += self.name_table_account
            # sqlaccount_match += " WHERE user_id = {}".format(int (peple_match))
            # print ("sqlaccount_match: " , sqlaccount_match)
            #     #mycursor.execute(sqluser, int (myresult[0][3]))
            # mycursor.execute(sqlaccount_match)

            # account_result_match = mycursor.fetchall()

            # # list_key = ['age','birthday','weight','height','bmi','job','movie','travel','shopping','swimming','camping','pet']
            # # query_json = {}
            # # for key in list_key:
            # #     query_json[key] = request.json.get(key)
            # # out = self.data_format_information.normalize(query_json)
            # # print (out)
            # return response.json({
            #     "status": 0,
            #     "msg": "Success",
            #     "name": user_result_match[0][1],
            #     "address": user_result_match[0][2],
            #     "phone": user_result_match[0][3],
            #     "gender": user_result_match[0][4],
            #     "birthday": user_result_match[0][5],
            #     "height": user_result_match[0][6],
            #     "weight": user_result_match[0][7],
            #     "job": user_result_match[0][8],
            #     "movie": user_result_match[0][9],
            #     "travel": user_result_match[0][10],
            #     "shopping": user_result_match[0][11],
            #     "swimming": user_result_match[0][12],
            #     "camping": user_result_match[0][13],
            #     "pet": user_result_match[0][14],
            #     "other_hobby": user_result_match[0][15],
            #     "username": account_result_match[0][1]
            # })
        


        # updateAccount
        @custom_webhook.route("/update_account", methods = ["POST"])
        async def update_account (request: Request) -> HTTPResponse:
            self.connect.ping(reconnect=True)
            
            username = request.json.get("username")
            password = request.json.get("password")

            name = request.json.get("name")
            address = request.json.get("address")
            phone = request.json.get("phone")
            gender = request.json.get("gender")
            birthday = request.json.get("birthday")
            height = request.json.get("height")
            weight = request.json.get("weight")
            job = request.json.get("job")
            movie = request.json.get("movie")
            travel = request.json.get("travel")
            shopping = request.json.get("shopping")
            swimming = request.json.get("swimming")
            camping = request.json.get("camping")
            pet = request.json.get("pet")
            other_hobby = request.json.get("other_hobby")

            print("username", username)
            print("password", password)
            print("pet", pet)

         
            mycursor = self.connect.cursor()

            sql =  "SELECT user_id FROM "
            sql += self.name_table_account; 
            sql += " WHERE username like %s"
            print(sql)
            mycursor.execute(sql, (str(username),))
           
        
            
            my_result = mycursor.fetchall()
            print(my_result)
            user_id = my_result[0][0]
            print(user_id)
            
            sqlChangePassword = "UPDATE "
            sqlChangePassword += self.name_table_account
            sqlChangePassword += " SET password = %s WHERE user_id = {}".format(int (user_id))
            print(sqlChangePassword)
            mycursor.execute(sqlChangePassword, (str(password),) )
            self.connect.commit()
            sqlChangeInformation = "UPDATE "
            sqlChangeInformation += self.name_table_user
            sqlChangeInformation += " SET name = %s, address = %s, phone = %s, gender = %s, birth_day = %s, height = %s, weight = %s, job = %s, movie = %s, travel = %s, shopping = %s, swimming = %s, camping = %s, pet = %s, other_hobby = %s WHERE ID = {}".format(int (user_id))
            print(sqlChangeInformation)
            mycursor.execute(sqlChangeInformation, (str(name), str(address), str(phone), str(gender), str(birthday), str(height), str(weight), str(job), str(movie), str(travel), str(shopping), str(swimming), str(camping), str(pet), str(other_hobby)))
            self.connect.commit()
            return response.json({
            "status": 0,
            "msg": "Success",    
            })
            # else:
            #      return response.json({
            #     "status": 0,
            #     "msg": "Failure",
            # })
        

        # inputSlots
        @custom_webhook.route("/inputSlots", methods=["GET"])
        async def input_slots(request: Request) -> HTTPResponse:
            list_slots = []
            for name_slot in self.reader_config_slots.get('slots'):
                list_slots.append(name_slot)

            return response.json({
                "status": 0,
                "msg": "Success",
                "inputSlots": list_slots,
            })

        # inputSlots
        @custom_webhook.route("/getTemplateAction", methods=["POST"])
        async def get_template_action(request: Request) -> HTTPResponse:
            # self.connect.ping(reconnect = True)
            # mycursor = self.connect.cursor()
            # sql = "SELECT * FROM "
            # sql += self.name_table_user + " WHERE id_conversation like %s"
            # id_conversation = request.json.get("id_conversation")
            # mycursor.execute(sql, [id_conversation])
            # myresult = mycursor.fetchall()
            # if len(myresult) > 0:
            #     for x in myresult:
            #         name = x[2]
            #         honor = x[3]
            #         phone_number = x[4]
            #         remain_money = x[5]
            #         used_money = x[6]
            #         date = x[7]
            # else :
            #     return response.json({
            #         "status": 0,
            #         "msg" : "Success",
            #         "action_template": [],
            #     })
            result = []
            for name_tmp in self.data_template:
                for temp in self.data_template[name_tmp]:
                    x = temp['text_tts']
                    if not x in result:
                        result.append(x)
            return response.json({
                "status": 0,
                "msg": "Success",
                "action_template": result,
            })

        # getStories
        @custom_webhook.route("/getStories", methods=["GET"])
        async def get_stories(request: Request) -> HTTPResponse:
            self.connect.ping(reconnect=True)
            mycursor = self.connect.cursor()
            sql = "SELECT pre_action, action_name, intent_name FROM "
            sql += self.name_table_account
            mycursor.execute(sql)

            myresult = mycursor.fetchall()
            check = {}
            total = 0

            for x in myresult:
                total += 1
                tmp = "/" + x[2]
                if check.get(x[0]) is None:
                    check[x[0]] = {}
                    check[x[0]][x[1]] = {}
                    check[x[0]][x[1]][tmp] = 1
                else:
                    if check.get(x[0]).get(x[1]) is None:
                        check[x[0]][x[1]] = {}
                        check[x[0]][x[1]][tmp] = 1
                    else:
                        if check.get(x[0]).get(x[1]).get(tmp) is None:
                            check[x[0]][x[1]][tmp] = 1
                        else:
                            check[x[0]][x[1]][tmp] += 1

            result_edges = self.result_stories

            for x in result_edges:
                pre_node = x['pre_node']
                cur_node = x['cur_node']
                intent = x['intent']
                if check.get(pre_node) is not None and check.get(pre_node).get(cur_node) is not None and check.get(
                        pre_node).get(cur_node).get(intent) is not None:
                    x['count'] = check.get(pre_node).get(cur_node).get(intent)
            list_nodes = ["START"] + self.domain.user_actions + ["END"]
            return response.json({
                "status": 0,
                "msg": "Success",
                "nodes": list_nodes,
                "edges": result_edges,
                "total_conversation": total
            })

        # # initialize of calling
        @custom_webhook.route("/initCall", methods=["POST"])
        async def get_slots(request: Request) -> HTTPResponse:
            self.connect.ping(reconnect=True)
            id_conversation = request.json.get("id_conversation")
            # input_slots = request.json.get("input_slots")
            # name = input_slots['name']
            # num_phone = input_slots['num_phone']
            # honor = input_slots['honor']
            # remain_amount = input_slots['remain_amount']
            # current_amount = input_slots['used_amount']
            # advance_date = input_slots['advance_date']

            mycursor = self.connect.cursor()
            sql = "INSERT INTO "
            sql += self.name_table_user + " (id_conversation) VALUES (%s)"
            val = (id_conversation,)
            mycursor.execute(sql, val)

            self.connect.commit()

            # list_slots = [ x.name for x in self.domain.slots ]
            return response.json({
                "status": 0,
                "msg": "Success",
            })

        # inputSlots
        @custom_webhook.route("/getConversation", methods=["POST"])
        async def get_conversation(request: Request) -> HTTPResponse:
            self.connect.ping(reconnect=True)
            id_conversation = request.json.get("id_conversation")
            if id_conversation is None:
                return response.json({
                    "status": -1,
                    "msg": "Fail",
                    "conversation": None,
                })
            mycursor = self.connect.cursor()
            sql = "SELECT pre_action, timestamp, intent_name, action_name, text_user, text_bot FROM "
            sql += self.name_table_account + " WHERE sender_id like %s"
            mycursor.execute(sql, [id_conversation])
            myresult = mycursor.fetchall()
            result = []
            for x in myresult:
                bot_json = json.loads(x[5])
                if x[3] == "action_start" and x[2] == "null_intent":
                    result = []
                    result.append({
                        "pre_action": "START",
                        "timestamp": x[1],
                        "intent_name": x[2],
                        "action_name": x[3],
                        "text_user": x[4],
                        "text_bot": bot_json['text'],
                        "text_tts_bot": bot_json['text_tts'],
                        "status_bot": bot_json['status']
                    })
                else:
                    result.append({
                        "pre_action": x[0],
                        "timestamp": x[1],
                        "intent_name": x[2],
                        "action_name": x[3],
                        "text_user": x[4],
                        "text_bot": bot_json['text'],
                        "text_tts_bot": bot_json['text_tts'],
                        "status_bot": bot_json['status']
                    })
            return response.json({
                "status": 0,
                "msg": "Success",
                "conversation": result,
            })

        @custom_webhook.route("/notuse", methods=["POST"])
        async def receive(request: Request) -> HTTPResponse:
            sender_id = await self._extract_sender(request)
            text = self._extract_message(request)
            should_use_stream = rasa.utils.endpoints.bool_arg(
                request, "stream", default=False
            )
            input_channel = self._extract_input_channel(request)
            metadata = self.get_metadata(request)

            if should_use_stream:
                return response.stream(
                    self.stream_response(
                        on_new_message, text, sender_id, input_channel, metadata
                    ),
                    content_type="text/event-stream",
                )
            else:
                collector = CollectingOutputChannel()
                # noinspection PyBroadException
                try:
                    await on_new_message(
                        UserMessage(
                            text,
                            collector,
                            sender_id,
                            input_channel=input_channel,
                            metadata=metadata,
                        )
                    )
                except CancelledError:
                    logger.error(
                        "Message handling timed out for "
                        "user message '{}'.".format(text)
                    )
                except Exception:
                    logger.exception(
                        "An exception occured while handling "
                        "user message '{}'.".format(text)
                    )
                return response.json(collector.messages)

        return custom_webhook
