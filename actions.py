# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/core/actions/#custom-actions/


# This is a simple example for a custom action which utters "Hello World!"

# from typing import Any, Text, Dict, List
#
# from rasa_sdk import Action, Tracker
# from rasa_sdk.executor import CollectingDispatcher
#
#
from typing import Text

from rasa_sdk import Action
from rasa_sdk.events import Restarted
import json
import logging
import copy

from src.utils.recorder import RecordManager
from config import *

logger = logging.getLogger(__name__)

with open(PATH_CONFIG_TEMPLATE_ACTION, "r") as file_action:
    action_template = json.load(file_action)

record_manager = RecordManager()


def execute_normal_action(tracker, cur_action):
    """
    execute action for all actions includes:
    - update records from tracker
    - get action response from action_template
    - format response message
    :param tracker: rasa tracker
    :param cur_action: current action
    :return: response
    """
    global action_template, record_manager
    responses = copy.deepcopy(action_template[cur_action])
    cur_intent = tracker.latest_message['intent'].get('name')
    recorder = record_manager.get(tracker)
    recorder.update(tracker, cur_action, cur_intent)
    pre_action = recorder.pre_action
    record = recorder.get_current_record()

    def get_action_response():
        response_ans = responses[0]
        for response_id in responses:
            if "properties" in response_id:
                properties = response_id["properties"]
                check_properties = True
                for p in properties:
                    value = properties[p]
                    # logger.debug("=" * 50 + str(properties) + " " + str(record[p]))
                    if p in record:
                        if isinstance(record[p], dict):
                            value_recorded = record[p]["value"]
                        else:
                            value_recorded = record[p]
                        if value_recorded != value:
                            check_properties = False
                            break
                if check_properties:
                    response_ans = response_id
                    break
            else:
                response_ans = response_id
                break
        # logger.debug("="*50 + str(response_ans))
        return response_ans

    def format_text(text, record):
        for key in record:
            if isinstance(record[key], dict):
                text = text.replace("{" + str(key) + "}", str(record[key]["text"]))
            else:
                text = text.replace("{" + str(key) + "}", str(record[key]))
        return text

    logging.debug(record)
    response = get_action_response()
    text = response["text"]
    text_tts = response["text_tts"]
    # text_tts = text  # get_action_response(pre_action, "text_tts")

    response["text"] = format_text(text, record)
    response["text_tts"] = format_text(text_tts, record)
    response["action"] = cur_action
    response["intent"] = cur_intent

    restart = response["status"] == "END" or response["status"] == "FORWARD"

    return json.dumps(response), restart


class action_quit(Action):
    def name(self) -> Text:
        return "action_quit"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []
        
class action_hello(Action):
    def name(self) -> Text:
        return "action_hello"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []

class action_fallback(Action):
    def name(self) -> Text:
        return "action_fallback"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []       

class action_bye(Action):
    def name(self) -> Text:
        return "action_bye"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []

class action_aboutme(Action):
    def name(self) -> Text:
        return "action_aboutme"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []
        
class action_affim(Action):
    def name(self) -> Text:
        return "action_affim"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []

class action_insult(Action):
    def name(self) -> Text:
        return "action_insult"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []

class action_sad(Action):
    def name(self) -> Text:
        return "action_sad"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []

class action_goodbot(Action):
    def name(self) -> Text:
        return "action_goodbot"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []

class action_whatislove(Action):
    def name(self) -> Text:
        return "action_whatislove"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []

class action_iamboy(Action):
    def name(self) -> Text:
        return "action_iamboy"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []

class action_iamgirl(Action):
    def name(self) -> Text:
        return "action_iamgirl"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []

class action_baitingboy(Action):
    def name(self) -> Text:
        return "action_baitingboy"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []

class action_gender_question(Action):
    def name(self) -> Text:
        return "action_gender_question"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []

class action_baitinggirl(Action):
    def name(self) -> Text:
        return "action_baitinggirl"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return [] 

class action_reponse_fallinlove(Action):
    def name(self) -> Text:
        return "action_response_fallinlove"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []

class action_respone_botability(Action):
    def name(self) -> Text:
        return "action_response_botability"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []        

class action_respone_iamfine(Action):
    def name(self) -> Text:
        return "action_response_iamfine"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []       

class action_chitchat1(Action):
    def name(self) -> Text:
        return "action_chitchat1"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []

class action_chitchat2(Action):
    def name(self) -> Text:
        return "action_chitchat2"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []

class action_chitchat3(Action):
    def name(self) -> Text:
        return "action_chitchat3"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []

class action_chitchat4(Action):
    def name(self) -> Text:
        return "action_chitchat4"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []


class action_chitchat5(Action):
    def name(self) -> Text:
        return "action_chitchat5"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []

class action_chitchat6(Action):
    def name(self) -> Text:
        return "action_chitchat6"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []

class action_chitchat7(Action):
    def name(self) -> Text:
        return "action_chitchat7"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []

class action_chitchat8(Action):
    def name(self) -> Text:
        return "action_chitchat8"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []

class action_chitchat9(Action):
    def name(self) -> Text:
        return "action_chitchat9"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []

class action_chitchat10(Action):
    def name(self) -> Text:
        return "action_chitchat10"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []

class action_chitchat11(Action):
    def name(self) -> Text:
        return "action_chitchat11"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []
class action_chitchat12(Action):
    def name(self) -> Text:
        return "action_chitchat12"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []
        
class action_chitchat13(Action):
    def name(self) -> Text:
        return "action_chitchat13"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []

class action_chitchat14(Action):
    def name(self) -> Text:
        return "action_chitchat14"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []
        
class action_chitchat15(Action):
    def name(self) -> Text:
        return "action_chitchat15"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []   

class action_chitchat16(Action):
    def name(self) -> Text:
        return "action_chitchat16"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []

class action_chitchat17(Action):
    def name(self) -> Text:
        return "action_chitchat17"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []

class action_chitchat18(Action):
    def name(self) -> Text:
        return "action_chitchat18"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []

class action_chitchat19(Action):
    def name(self) -> Text:
        return "action_chitchat19"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []
class action_chitchat20(Action):
    def name(self) -> Text:
        return "action_chitchat20"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []
        
class action_chitchat21(Action):
    def name(self) -> Text:
        return "action_chitchat21"

    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []    

class action_response_matching_people(Action):
    def name(self) -> Text:
        return "action_response_matching_people"
        
    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []    

class action_special_height(Action):
    def name(self) -> Text:
        return "action_special_height"
        
    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []    

class action_special_appearance(Action):
    def name(self) -> Text:
        return "action_special_appearance"
        
    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return [] 

class action_special_job(Action):
    def name(self) -> Text:
        return "action_special_job"
        
    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return [] 


class action_result_matching(Action):
    def name(self) -> Text:
        return "action_result_matching"
        
    def run(self, dispatcher, tracker, domain):
        response, restart = execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []     